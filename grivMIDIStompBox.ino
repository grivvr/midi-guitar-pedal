/*
  grivs MIDI guitar pedal mod
  BOSS DD-3 Digital Delay

  3 pots (MIDI CC)
  1 pedal button (MIDI Note)
  4 position rotary switch
    - 1. MIDI Channel 1
    - 2. MIDI Channel 2
    - 3. MIDI Channel 1, press / release pedal button
    - 4. MIDI Channel 2, press / release pedal button

** The LED lights with the pedal press, but may need to be in 'sync' with your software button.

*/

#include <Bounce.h>

// the MIDI channel number to send messages
int channel = 1;

// pot value change threshold
const int analog_threshold = 2;

// pot scale (1024 / 128)
const int analog_scale = 8;

// pots
const int number_pots = 3;
const int analog_pin[] = { A1, A2, A3 };

// buttons
const int number_buttons = 5;
const int digital_pin[] = { 0, 1, 2, 3, 4 };

// button 0 bounce
Bounce button0 = Bounce(0, 5);  // 5 = 5 ms debounce time

// store last pot values
int analog_val_last[3];

// led pin
const int ledPin = 10;

// led brightness
const int ledBrightness = 4;

// led state
boolean ledState = false;


void setup() {

  // buttons
  for (int i=0; i<number_buttons; i++) {
    pinMode(i, INPUT_PULLUP);
  }
  
  // pots
  for (int i=0; i<number_pots; i++) {
    analog_val_last[i] = 0;
  }

  // turn led on
  pinMode(ledPin, OUTPUT);
  
}


void loop() {
  
  // use multi point switch to determine midi channel to send on
  channel = 1;
  if (digitalRead(2) == LOW) {
    channel = 2;
  } else if (digitalRead(3) == LOW) {
    channel = 3;
  } else if (digitalRead(4) == LOW) {
    channel = 4;
  } else if (digitalRead(5) == LOW) {
    channel = 1;
  }  
    
  // pots
  for (int i=0; i<number_pots; i++) {
    int current_val = analogRead(analog_pin[i]);
    if (abs(current_val - analog_val_last[i]) > analog_threshold) {
      usbMIDI.sendControlChange(i+1, current_val / 8, channel);
      analog_val_last[i] = current_val;
    }
  }
   
  // button 0
  button0.update();
  if (button0.fallingEdge()) {
    usbMIDI.sendNoteOn(60, 99, channel);  // 60 = C4
    ledState = !ledState;
    if (ledState) {
      analogWrite(ledPin, ledBrightness);
    } else {
      analogWrite(ledPin, 0);
    }
  } else if (button0.risingEdge() && channel >= 3) {
    usbMIDI.sendNoteOff(60, 99, channel);
    ledState = !ledState;
    if (ledState) {
      analogWrite(ledPin, ledBrightness);
    } else {
      analogWrite(ledPin, 0);
    }
  }

}
